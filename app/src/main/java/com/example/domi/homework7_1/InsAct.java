package com.example.domi.homework7_1;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class InsAct extends Activity implements View.OnClickListener {
    private Button btn1;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ins);

        btn1 = (Button) findViewById(R.id.button1);
        btn1.setOnClickListener(this);
    }

    public void onClick(View v) {
        EditText txt = null;
        txt = (EditText) findViewById(R.id.editText0);
        String sno = txt.getText().toString();
        if(sno.length()!=10) {
            Toast.makeText(this, "학번은 10자리 입니다.", Toast.LENGTH_LONG).show();
            return;
        }
        txt = (EditText) findViewById(R.id.editText1);
        String name = txt.getText().toString();
        if(name.length() <= 20) {
            Toast.makeText(this, "이름은 20자 이내 입니다.", Toast.LENGTH_LONG).show();
            return;
        }
        txt = (EditText) findViewById(R.id.editText2);
        String age = txt.getText().toString();
        if(Integer.parseInt(age) <= 200) {
            Toast.makeText(this, "나이는 200세 이하입니다", Toast.LENGTH_LONG).show();
            return;
        }
        String sql = "INSERT INTO student (sno,name,age) VALUES (" + sno + ",'" + name + "'," + age + ");";
        SQLiteDatabase db = openOrCreateDatabase("homework.db", SQLiteDatabase.CREATE_IF_NECESSARY, null);
        db.execSQL(sql);
        finish();
    }


}
