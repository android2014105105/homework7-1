package com.example.domi.homework7_1;

import android.app.Activity;
import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class Update extends Activity {
    private int option = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        loadDB();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadDB();
    }

    public void loadDB() {
        SQLiteDatabase db = openOrCreateDatabase("homework.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );
        db.execSQL("CREATE TABLE IF NOT EXISTS student " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT , sno INTEGER, name TEXT, age INTEGER);");

        Cursor c = db.rawQuery("SELECT * FROM student;", null);
        startManagingCursor(c);

        ListAdapter adapt = new SimpleCursorAdapter(
                this,
                R.layout.item,
                c, new String[]{"sno","name", "age"},
                new int[]{R.id.Text1, R.id.Text2, R.id.Text3}, 0
        );
        ListView listView = (ListView)findViewById(R.id.item);
        listView.setAdapter(adapt);

        if (db != null) {
            db.close();
        }
    }
    public void onClickButton1(View v) {
        option = 1;
    }
    public void onClickButton2(View v) {
        option = 2;
    }

    public void onClickButton(View v) {
        EditText txt = null;
        txt = (EditText) findViewById(R.id.editText3);
        String name = txt.getText().toString();
        txt = (EditText) findViewById(R.id.editText4);
        String age = txt.getText().toString();
        String sql = null;
        if(option == 1)
        {
            sql = "UPDATE student SET name = '" + name + "' WHERE age = "+ age +";";
        }
        else if(option == 2)
        {
            sql = "UPDATE student SET age = "+ age +" WHERE name = '" + name + "';";
        }
        SQLiteDatabase db = openOrCreateDatabase("homework.db", SQLiteDatabase.CREATE_IF_NECESSARY, null);
        db.execSQL(sql);
        db.close();
        loadDB();
    }
}


