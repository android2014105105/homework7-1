package com.example.domi.homework7_1;

import android.app.Activity;
import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class DelAct extends Activity implements View.OnClickListener {
    private Button btn1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_del);
        loadDB();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadDB();
    }

    public void loadDB() {

        SQLiteDatabase db = openOrCreateDatabase("homework.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );
        db.execSQL("CREATE TABLE IF NOT EXISTS student " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT , sno INTEGER, name TEXT, age INTEGER);");

        Cursor c = db.rawQuery("SELECT * FROM student;", null);
        startManagingCursor(c);

        ListAdapter adapt = new SimpleCursorAdapter(
                this,
                R.layout.item,
                c, new String[]{"sno","name", "age"},
                new int[]{R.id.Text1, R.id.Text2, R.id.Text3}, 0
        );
        ListView listView = (ListView)findViewById(R.id.item);
        listView.setAdapter(adapt);

        if (db != null) {
            db.close();
        }
    }

    public void onClickButton(View v) {
        EditText txt = null;
        txt = (EditText) findViewById(R.id.editText5);
        String sno = txt.getText().toString();
        String sql = "DELETE FROM student WHERE sno=" + sno + ";";
        SQLiteDatabase db = openOrCreateDatabase("homework.db", SQLiteDatabase.CREATE_IF_NECESSARY, null);
        db.execSQL(sql);
        db.close();
        loadDB();
    }

    @Override
    public void onClick(View v) {

        btn1 = (Button) findViewById(R.id.button7);
        btn1.setOnClickListener(this);

    }
}


