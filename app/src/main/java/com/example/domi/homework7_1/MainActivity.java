package com.example.domi.homework7_1;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SQLiteDatabase db = openOrCreateDatabase("homework.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );
        db.execSQL("CREATE TABLE IF NOT EXISTS student " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT , sno INTEGER, name TEXT, age INTEGER);");

        if (db != null) {
            db.close();
        }
    }


    public void showInsAct(View v) {
        Intent i = new Intent(this, InsAct.class);
        startActivity(i);
    }
    public void DeleteName(View v) {
        Intent i = new Intent(this, DelAct.class);
        startActivity(i);
    }
    public void Update(View v) {
        Intent i = new Intent(this, Update.class);
        startActivity(i);
    }
}
